package com.lntea.netty.chat;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Title: HuTongChatApplication.java<br>
 * Description:  <br>
 *
 * @author LC
 * @date 2020/9/24 17:16
 */
public class HuTongChatApplication {

    private CountDownLatch latch = new CountDownLatch(1);

    public static void main(String[] args) {
        new Thread(new ZhangServer(9999)).start();

    }

    @Test
    public void startClient() throws InterruptedException {
        new LiClient("127.0.0.1", 9999).run();
        // latch.await();
    }
}
