package com.lntea.netty.chat;

import java.io.Serializable;

/**
 * Title: HuTongMessage.java<br>
 * Description:  <br>
 *
 * @author LC
 * @date 2020/9/24 15:21
 */
public class HuTongMessage implements Serializable {
    /**
     * 消息标识ID
     */
    private int msgId;

    /**
     * 消息内容
     */
    private String msgContent;

    public HuTongMessage(int msgId, String msgContent) {
        this.msgId = msgId;
        this.msgContent = msgContent;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    @Override
    public String toString() {
        return "HuTongMessage{" +
                "msgId=" + msgId +
                ", msgContent='" + msgContent + '\'' +
                '}';
    }
}
