package com.lntea.netty.chat;

import java.util.HashMap;
import java.util.Map;

/**
 * Title: HuTongChatEnum.java<br>
 * Description:  <br>
 *
 * @author LC
 * @date 2020/9/24 15:15
 */
public enum HuTongChatEnum {
    ZHANG_3("回去给老太太请安。", null),
    LI_3("有空家里坐坐。", ZHANG_3),
    ZHANG_2("溜溜。", null),
    LI_2("嘛去？", ZHANG_2),
    LI_1("刚吃。", null),
    ZHANG_1("吃了没？", LI_1);

    private String dialog;
    private HuTongChatEnum next;

    private static Map<String, HuTongChatEnum> chatMap = new HashMap<>();
    static {
        chatMap.put(ZHANG_1.dialog, ZHANG_1);
        chatMap.put(ZHANG_2.dialog, ZHANG_2);
        chatMap.put(ZHANG_3.dialog, ZHANG_3);
        chatMap.put(LI_1.dialog, LI_1);
        chatMap.put(LI_2.dialog, LI_2);
        chatMap.put(LI_3.dialog, LI_3);
    }

    HuTongChatEnum(String dialog, HuTongChatEnum next) {
        this.next = next;
        this.dialog = dialog;
    }

    public String getDialog() {
        return dialog;
    }

    /**
     * 下一行对白
     * @param dialog
     * @return
     */
    public static String nextDialog(String dialog) {
        HuTongChatEnum next = chatMap.get(dialog).next;
        if (next == null) {
            return null;
        }
        return next.dialog;
    }
}
