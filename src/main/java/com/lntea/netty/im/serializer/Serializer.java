package com.lntea.netty.im.serializer;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据序列化
 */
public interface Serializer {

    Serializer DEFAULT = new JSONSerializer();

    Map<Byte, Serializer> SERIALIZER_MAP = new HashMap(){{
        put(SerializerAlgorithm.JSON, Serializer.DEFAULT);
    }};

    /**
     * 序列化算法
     * @return
     */
    byte getSerializerAlgorithm();

    /**
     * 对象转换为二进制
     * @param object
     * @return
     */
    byte[] serializer(Object object);

    /**
     * 二进制转换为对象
     * @param clazz
     * @param bytes
     * @param <T>
     * @return
     */
    <T> T deserializer(Class<T> clazz, byte[] bytes);
}
