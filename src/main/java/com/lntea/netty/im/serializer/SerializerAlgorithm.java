package com.lntea.netty.im.serializer;

/**
 * 序列化算法常量
 */
public interface SerializerAlgorithm {

    /**
     * json序列化
     */
    byte JSON = 1;
}
