package com.lntea.netty.im.command;

import com.lntea.netty.im.packet.CreateGroupRequestPacket;
import com.lntea.netty.im.packet.GroupMessageRequestPacket;
import com.lntea.netty.im.packet.common.SessionUtil;
import io.netty.channel.Channel;

import java.util.Scanner;

/**
 * 发送群消息
 */
public class SendToGroupConsoleCommand implements ConsoleCommand {

    @Override
    public void exec(Scanner scanner, Channel channel) {
        GroupMessageRequestPacket packet = new GroupMessageRequestPacket();

        System.out.println("输入消息：格式为groupId message");
        String fromUserId = SessionUtil.getSession(channel).getUserId();
        String groupId = scanner.next();
        String message = scanner.nextLine();
        packet.setFromUserId(fromUserId);
        packet.setGroupId(groupId);
        packet.setMessage(message);
        channel.writeAndFlush(packet);
    }
}
