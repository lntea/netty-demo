package com.lntea.netty.im.command;

import com.lntea.netty.im.packet.common.SessionUtil;
import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 控制台命令管理器
 */
public class ConsoleCommandManager {

    private Map<String, ConsoleCommand> commandMap = new HashMap<>();

    public ConsoleCommandManager() {
        commandMap.put("login", new LoginConsoleCommand());
        commandMap.put("s2u", new SendToUserConsoleCommand());
        commandMap.put("createGroup", new CreateGroupConsoleCommand());
        commandMap.put("s2g", new SendToGroupConsoleCommand());
    }

    /**
     * 执行控制台命令
     * @param scanner
     * @param channel
     */
    public void exec(Scanner scanner, Channel channel) {
        String command = scanner.next();
        ConsoleCommand consoleCommand = commandMap.get(command);
        if (consoleCommand == null) {
            System.out.println("无法识别" + command + "命令，请重新输入");
        } else {
            consoleCommand.exec(scanner, channel);
        }
    }
}
