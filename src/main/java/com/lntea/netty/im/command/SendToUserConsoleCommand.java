package com.lntea.netty.im.command;

import com.lntea.netty.im.packet.MessageRequestPacket;
import io.netty.channel.Channel;

import java.util.Scanner;

/**
 * 用户单聊命令
 */
public class SendToUserConsoleCommand implements ConsoleCommand {

    @Override
    public void exec(Scanner scanner, Channel channel) {
        MessageRequestPacket packet = new MessageRequestPacket();

        System.out.println("输入消息：格式为userId message");
        String toUserId = scanner.next();
        String message = scanner.next();
        packet.setToUserId(toUserId);
        packet.setMessage(message);
        channel.writeAndFlush(packet);
    }
}
