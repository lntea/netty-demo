package com.lntea.netty.im.command;

import io.netty.channel.Channel;

import java.util.Scanner;

/**
 * 控制台命令
 */
public interface ConsoleCommand {

    /**
     * 执行控制台命令
     * @param scanner
     * @param channel
     */
    void exec(Scanner scanner, Channel channel);
}
