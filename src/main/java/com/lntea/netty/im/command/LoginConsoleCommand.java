package com.lntea.netty.im.command;

import com.lntea.netty.im.packet.LoginRequestPacket;
import com.lntea.netty.im.packet.LoginResponsePacket;
import io.netty.channel.Channel;

import java.util.Scanner;

/**
 * 登录命令
 */
public class LoginConsoleCommand implements ConsoleCommand{

    @Override
    public void exec(Scanner scanner, Channel channel) {
        LoginRequestPacket packet = new LoginRequestPacket();

        System.out.println("登录，请输入用户名：");
        String userName = scanner.next();
        packet.setUserName(userName);
        packet.setPassword("pwd");
        channel.writeAndFlush(packet);
    }
}
