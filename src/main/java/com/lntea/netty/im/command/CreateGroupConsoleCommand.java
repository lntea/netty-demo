package com.lntea.netty.im.command;

import com.lntea.netty.im.packet.CreateGroupRequestPacket;
import io.netty.channel.Channel;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 创建群组命令
 */
public class CreateGroupConsoleCommand implements ConsoleCommand{

    public static final String USER_ID_SPLITER = ",";

    @Override
    public void exec(Scanner scanner, Channel channel) {
        CreateGroupRequestPacket packet = new CreateGroupRequestPacket();

        System.out.println("[拉人群聊]输入群成员userId列表，以英文逗号分隔：");
        String userIds = scanner.next();
        packet.setUserIdList(Arrays.asList(userIds.split(USER_ID_SPLITER)));
        channel.writeAndFlush(packet);
    }
}
