package com.lntea.netty.im.handler;

import com.lntea.netty.im.packet.LoginResponsePacket;
import com.lntea.netty.im.packet.common.Session;
import com.lntea.netty.im.packet.common.SessionUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Date;

/**
 * 登录响应处理Handler
 */
public class LoginResponseHandler extends SimpleChannelInboundHandler<LoginResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginResponsePacket respPacket) throws Exception {
        if (respPacket.getSuccess()) {
            // 登录成功
            String userId = respPacket.getUserId();
            String userName = respPacket.getUserName();
            System.out.println(String.format("用户%s[%s]登陆成功", userName, userId));
            SessionUtil.bindSession(new Session(userId, userName), ctx.channel());
        } else {
            // 登录失败
            System.out.println(String.format("用户%s登陆失败，原因：%s", respPacket.getUserName(), respPacket.getReason()));
        }
    }
}
