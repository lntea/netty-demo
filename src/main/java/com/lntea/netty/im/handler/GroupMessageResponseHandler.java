package com.lntea.netty.im.handler;

import com.lntea.netty.im.packet.GroupMessageResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 群消息响应Handler
 */
public class GroupMessageResponseHandler extends SimpleChannelInboundHandler<GroupMessageResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupMessageResponsePacket packet) throws Exception {
        System.out.println(String.format("群[%s]中[%s]说：[%s]", packet.getGroupId(),
                packet.getFromUserName(), packet.getMessage()));
    }
}
