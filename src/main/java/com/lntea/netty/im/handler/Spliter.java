package com.lntea.netty.im.handler;

import com.lntea.netty.im.codec.PacketCodeC;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * 自定义拆包器
 */
public class Spliter extends LengthFieldBasedFrameDecoder {

    private static final Integer LENGTH_FIELD_OFFSET = 7;
    private static final Integer LENGTH_FIELD_LENGTH = 4;


    public Spliter() {
        super(Integer.MAX_VALUE, LENGTH_FIELD_OFFSET, LENGTH_FIELD_LENGTH);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        // 屏蔽非本协议客户端连接
        if (in.getInt(in.readerIndex()) != PacketCodeC.MAGIC_NUMBER) {
            ctx.channel().close();
        }

        return super.decode(ctx, in);
    }
}
