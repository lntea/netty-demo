package com.lntea.netty.im.handler;

import com.lntea.netty.im.packet.HeartBeatRequestPacket;
import com.lntea.netty.im.packet.HeartBeatResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 心跳响应Handler
 */
public class HeartBeatTimerResponseHandler extends SimpleChannelInboundHandler<HeartBeatRequestPacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HeartBeatRequestPacket packet) throws Exception {
        System.out.println("接收心跳消息");
        ctx.channel().writeAndFlush(new HeartBeatResponsePacket());
    }
}
