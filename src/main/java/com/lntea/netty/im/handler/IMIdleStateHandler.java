package com.lntea.netty.im.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

public class IMIdleStateHandler extends IdleStateHandler {

    private static final int readerIdleTimeSeconds = 15;

    public IMIdleStateHandler() {
        super(readerIdleTimeSeconds, 0, 0, TimeUnit.SECONDS);
    }

    @Override
    protected void channelIdle(ChannelHandlerContext ctx, IdleStateEvent evt) throws Exception {
        System.out.println(String.format("%s秒内未读到数据，关闭连接", readerIdleTimeSeconds));
        ctx.channel().close();
    }
}
