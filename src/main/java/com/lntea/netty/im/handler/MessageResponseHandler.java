package com.lntea.netty.im.handler;

import com.lntea.netty.im.packet.MessageResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Date;

/**
 * 消息响应处理Handler
 */
public class MessageResponseHandler extends SimpleChannelInboundHandler<MessageResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageResponsePacket respPacket) throws Exception {
        String fromUserId = respPacket.getFromUserId();
        String fromUserName = respPacket.getFromUserName();
        System.out.println(String.format("%s[%s] : %s", fromUserName, fromUserId, respPacket.getMessage()));
    }
}
