package com.lntea.netty.im.handler;

import com.lntea.netty.im.packet.CreateGroupResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 创建群组响应Handler
 */
public class CreateGroupResponseHandler extends SimpleChannelInboundHandler<CreateGroupResponsePacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, CreateGroupResponsePacket packet) throws Exception {
        System.out.println(String.format("群创建成功，id[%s]", packet.getGroupId()));
        System.out.println(String.format("群成员为%s", packet.getUserNameList()));
    }
}
