package com.lntea.netty.im.packet;

import lombok.Data;

/**
 * rpc数据包
 */
@Data
public abstract class Packet {

    /**
     * 协议版本
     */
    private Byte version = 1;

    /**
     * 获取指定类型
     * @return
     */
    public abstract Byte getCommand();
}
