package com.lntea.netty.im.packet.common;

import lombok.Data;

/**
 * 会话
 */
@Data
public class Session {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    public Session(String userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }
}
