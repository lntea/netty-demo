package com.lntea.netty.im.packet.common;

import com.lntea.netty.im.packet.*;

import java.util.HashMap;
import java.util.Map;

/**
 * rpc数据包指令
 */
public interface Command {

    /**
     * login request
     */
    Byte LOGIN_REQUEST = 1;
    /**
     * login response
     */
    Byte LOGIN_RESPONSE = 2;
    /**
     * message request
     */
    Byte MESSAGE_REQUEST = 3;
    /**
     * message response
     */
    Byte MESSAGE_RESPONSE = 4;
    /**
     * create group req
     */
    Byte CREATE_GROUP_REQUEST = 5;
    /**
     * create group resp
     */
    Byte CREATE_GROUP_RESPONSE = 6;
    /**
     * group message req
     */
    Byte GROUP_MESSAGE_REQUEST = 7;
    /**
     * group message response
     */
    Byte GROUP_MESSAGE_RESPONSE = 8;
    /**
     * heartbeat request
     */
    Byte HEARTBEAT_REQUEST = 9;
    /**
     * heartbeat_response
     */
    Byte HEARTBEAT_RESPONSE = 10;

    Map<Byte, Class<? extends Packet>> COMMAND_MAP = new HashMap(){{
        put(LOGIN_REQUEST, LoginRequestPacket.class);
        put(LOGIN_RESPONSE, LoginResponsePacket.class);
        put(MESSAGE_REQUEST, MessageRequestPacket.class);
        put(MESSAGE_RESPONSE, MessageResponsePacket.class);
        put(CREATE_GROUP_REQUEST, CreateGroupRequestPacket.class);
        put(CREATE_GROUP_RESPONSE, CreateGroupResponsePacket.class);
        put(GROUP_MESSAGE_REQUEST, GroupMessageRequestPacket.class);
        put(GROUP_MESSAGE_RESPONSE, GroupMessageResponsePacket.class);
        put(HEARTBEAT_REQUEST, HeartBeatRequestPacket.class);
        put(HEARTBEAT_RESPONSE, HeartBeatResponsePacket.class);
    }};
}
