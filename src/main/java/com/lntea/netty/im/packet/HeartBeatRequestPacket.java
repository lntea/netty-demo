package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;

/**
 * 心跳数据包
 */
public class HeartBeatRequestPacket extends Packet {

    @Override
    public Byte getCommand() {
        return Command.HEARTBEAT_REQUEST;
    }
}
