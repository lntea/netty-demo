package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import lombok.Data;

@Data
public class MessageRequestPacket extends Packet {

    /**
     * 接收人
     */
    private String toUserId;
    /**
     * 消息内容
     */
    private String message;

    public MessageRequestPacket() {
    }

    public MessageRequestPacket(String toUserId, String message) {
        this.toUserId = toUserId;
        this.message = message;
    }

    @Override
    public Byte getCommand() {
        return Command.MESSAGE_REQUEST;
    }
}
