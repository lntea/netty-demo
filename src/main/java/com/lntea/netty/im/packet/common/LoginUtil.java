package com.lntea.netty.im.packet.common;

import io.netty.channel.Channel;
import io.netty.util.Attribute;

/**
 * 登录校验工具
 */
public class LoginUtil {

    /**
     * 标记登录状态
     * @param channel
     */
    public static void markAsLogin(Channel channel) {
        channel.attr(Attributes.LOGIN).set(true);
    }

    /**
     * 查询是否登录
     * @param channel
     * @return
     */
    public static boolean hasLogin(Channel channel) {
        Attribute<Boolean> attr = channel.attr(Attributes.LOGIN);
        if (attr.get() == null) {
            return false;
        }
        return true;
    }
}
