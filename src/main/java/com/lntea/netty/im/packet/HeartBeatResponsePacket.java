package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;

/**
 * 心跳响应数据包
 */
public class HeartBeatResponsePacket extends Packet {

    @Override
    public Byte getCommand() {
        return Command.HEARTBEAT_RESPONSE;
    }
}
