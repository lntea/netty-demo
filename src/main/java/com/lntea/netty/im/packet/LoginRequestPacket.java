package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import lombok.Data;

/**
 * 登录请求数据包
 */
@Data
public class LoginRequestPacket extends Packet{

    private String userId;

    private String userName;

    private String password;

    public LoginRequestPacket() {
    }

    public LoginRequestPacket(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    @Override
    public Byte getCommand() {
        return Command.LOGIN_REQUEST;
    }
}
