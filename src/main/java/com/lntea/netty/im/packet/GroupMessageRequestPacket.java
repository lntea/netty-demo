package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import lombok.Data;

/**
 * 群消息请求数据包
 */
@Data
public class GroupMessageRequestPacket extends Packet {

    /**
     * 发送人id
     */
    private String fromUserId;
    /**
     * 群组id
     */
    private String groupId;
    /**
     * 群消息体
     */
    private String message;

    @Override
    public Byte getCommand() {
        return Command.GROUP_MESSAGE_REQUEST;
    }
}
