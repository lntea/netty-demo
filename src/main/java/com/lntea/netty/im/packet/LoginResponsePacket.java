package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import lombok.Data;

/**
 * 登录响应数据包
 */
@Data
public class LoginResponsePacket extends Packet {

    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 错误原因
     */
    private String reason;

    @Override
    public Byte getCommand() {
        return Command.LOGIN_RESPONSE;
    }
}
