package com.lntea.netty.im.packet.common;

import io.netty.util.AttributeKey;

/**
 * channel属性常量
 */
public interface Attributes {

    AttributeKey<Boolean> LOGIN = AttributeKey.newInstance("login");

    AttributeKey<Session> SESSION = AttributeKey.newInstance("session");
}
