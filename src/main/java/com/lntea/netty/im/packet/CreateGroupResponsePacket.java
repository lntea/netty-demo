package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import com.lntea.netty.im.packet.common.Session;
import lombok.Data;

import java.util.List;

@Data
public class CreateGroupResponsePacket extends Packet{

    /**
     * 是否成功
     */
    private Boolean success;
    /**
     * 群组id
     */
    private String groupId;
    /**
     * 群组会话集合
     */
    private List<String> userNameList;

    @Override
    public Byte getCommand() {
        return Command.CREATE_GROUP_RESPONSE;
    }
}
