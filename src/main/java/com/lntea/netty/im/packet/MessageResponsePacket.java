package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import lombok.Data;

@Data
public class MessageResponsePacket extends Packet {

    /**
     * 发送人id
     */
    private String fromUserId;

    /**
     * 发送人名称
     */
    private String fromUserName;

    /**
     * 消息内容
     */
    private String message;

    public MessageResponsePacket() {
    }

    public MessageResponsePacket(String fromUserId, String fromUserName) {
        this.fromUserId = fromUserId;
        this.fromUserName = fromUserName;
    }

    @Override
    public Byte getCommand() {
        return Command.MESSAGE_RESPONSE;
    }
}
