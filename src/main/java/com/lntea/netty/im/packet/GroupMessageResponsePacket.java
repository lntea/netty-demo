package com.lntea.netty.im.packet;

import com.lntea.netty.im.packet.common.Command;
import lombok.Data;

/**
 * 群消息响应数据包
 */
@Data
public class GroupMessageResponsePacket extends Packet{

    /**
     * 发送人id
     */
    private String fromUserId;
    /**
     * 发送人名称
     */
    private String fromUserName;
    /**
     * 群组id
     */
    private String groupId;
    /**
     * 群消息
     */
    private String message;

    @Override
    public Byte getCommand() {
        return Command.GROUP_MESSAGE_RESPONSE;
    }
}
