package com.lntea.netty.im;

import com.lntea.netty.im.codec.PacketDecoder;
import com.lntea.netty.im.codec.PacketEncoder;
import com.lntea.netty.im.handler.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class IMServer2 {

    public static void main(String[] args) {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap
                .group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        // ch.pipeline().addLast(new FirstServerHandler());
                        // ch.pipeline().addLast(new ServerHandler());
                        ch.pipeline()
                                .addLast(new IMIdleStateHandler())
                                .addLast(new Spliter())
                                .addLast(new PacketDecoder())
                                .addLast(new HeartBeatTimerResponseHandler())
                                .addLast(new LoginRequestHandler())
                                .addLast(new AuthHandler())
                                .addLast(new CreateGroupRequestHandler())
                                .addLast(new GroupMessageRequestHandler())
                                .addLast(new MessageRequestHandler())
                                .addLast(new PacketEncoder());
                    }
                });

        bootstrap.bind(9999).addListener(future -> {
           if (future.isSuccess()) {
               System.out.println("端口9999服务启动成功");
           } else {
               System.out.println("端口9999服务启动失败");
           }
        });
    }
}
