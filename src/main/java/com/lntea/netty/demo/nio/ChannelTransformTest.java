package com.lntea.netty.demo.nio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;

import org.junit.Test;

public class ChannelTransformTest {

    @Test
    public void testTransform() throws FileNotFoundException, URISyntaxException{
        RandomAccessFile fromFile = new RandomAccessFile(new File(FileChannelTest.class.getResource("fromFile.txt").toURI()), "rw");
        FileChannel fromChannel = fromFile.getChannel();
        
        RandomAccessFile toFile = new RandomAccessFile(new File(FileChannelTest.class.getResource("toFile.txt").toURI()), "rw");
        FileChannel toChannel = toFile.getChannel();
        
        
        try {
            toChannel.transferFrom(fromChannel, 0, fromChannel.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testTransTo() throws FileNotFoundException, URISyntaxException{
        RandomAccessFile fromFile = new RandomAccessFile(new File(FileChannelTest.class.getResource("fromFile.txt").toURI()), "rw");
        FileChannel fromChannel = fromFile.getChannel();
        
        RandomAccessFile toFile = new RandomAccessFile(new File(FileChannelTest.class.getResource("toFile.txt").toURI()), "rw");
        FileChannel toChannel = toFile.getChannel();
        
        
        try {
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
