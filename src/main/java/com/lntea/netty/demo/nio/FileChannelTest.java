package com.lntea.netty.demo.nio;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelTest {
	public static void main(String[] args) throws IOException, URISyntaxException {
		RandomAccessFile file = new RandomAccessFile(new File(FileChannelTest.class.getResource("input.txt").toURI()), "rw");
		
		FileChannel fileChannel = file.getChannel();
		
		ByteBuffer buffer = ByteBuffer.allocate(48);
		int read = fileChannel.read(buffer);
		
		while(read!=-1){
			System.out.println("read:"+read);
			buffer.flip();
			
			while(buffer.hasRemaining()){
				System.out.println((char)buffer.get());
			}
			
			buffer.clear();
			read = fileChannel.read(buffer);
		}
		
		file.close();
	}
}
