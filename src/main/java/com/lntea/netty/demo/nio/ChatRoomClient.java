package com.lntea.netty.demo.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class ChatRoomClient {

    private Selector selector = null;
    
    private SocketChannel sc = null;
    
    private int port = 9999;
    
    private String name = "";
    
    private Charset charset = Charset.forName("utf-8");
    
    private static final String USER_CONTENT_SPLIT = "#@#";

    private static final String USER_EXIST = "user existed";
    
    public static void main(String[] args) {
        try {
            new ChatRoomClient().init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void init() throws IOException{
        selector = Selector.open();
        sc = SocketChannel.open(new InetSocketAddress("127.0.0.01", port));
        sc.configureBlocking(false);
        sc.register(selector, SelectionKey.OP_READ);
        
        new Thread(new ClientThread()).start();
        
        Scanner scan = new Scanner(System.in);
        while(scan.hasNextLine()){
            String line = scan.nextLine();
            if("".equals(line)){
                continue;
            }
            if("".equals(name)){
                name = line;
                line = name+USER_CONTENT_SPLIT;
            }else{
                line = name + USER_CONTENT_SPLIT + line;
            }
            sc.write(charset.encode(line));
        }
    }
    
    
    class ClientThread implements Runnable{

        public void run() {
            while(true){
                int readyChannels = 0;
                try {
                    readyChannels = selector.select();
                } catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
                if(readyChannels == 0)
                    continue;
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> ite = keys.iterator();
                while(ite.hasNext()){
                    SelectionKey key = ite.next();
                    ite.remove();
                    try {
                        processSelectionKey(key);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void processSelectionKey(SelectionKey key) throws IOException {
            if(key.isReadable()){
                SocketChannel sc = (SocketChannel) key.channel();
                ByteBuffer buff = ByteBuffer.allocate(1024);
                String content = "";
                while(sc.read(buff)>0){
                    buff.flip();
                    content += charset.decode(buff);
                }
                
                if(USER_EXIST.equals(content)){
                    name = "";
                }
                System.out.println(content);
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }
}
