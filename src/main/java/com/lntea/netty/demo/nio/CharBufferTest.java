package com.lntea.netty.demo.nio;

import java.io.IOException;
import java.nio.CharBuffer;

public class CharBufferTest {
    public static void main(String[] args) throws IOException {

        CharBuffer buffer = CharBuffer.allocate(5);
        buffer.put("天涯若比邻");
        
        buffer.flip();
        
        StringBuffer sb = new StringBuffer();
        boolean isMarked = false;
        while(buffer.hasRemaining()){
            char i = buffer.get();
            if(i=='若'&&!isMarked){
                buffer.mark();
            }
            if(i=='邻'&&!isMarked){
                buffer.reset();
                isMarked = true;
            }
            System.out.println(i);
            sb.append(i);
        }
        System.out.println(sb.toString());
    }
}
