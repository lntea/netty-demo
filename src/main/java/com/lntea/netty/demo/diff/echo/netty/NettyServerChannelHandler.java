package com.lntea.netty.demo.diff.echo.netty;

import com.lntea.netty.demo.diff.echo.EchoConstant;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

import java.util.Calendar;

public class NettyServerChannelHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buf = (ByteBuf) msg;
        String body = buf.toString(CharsetUtil.UTF_8);
        System.out.println(String.format("Echo Server receive msg: %s",body));
        String responseBody = "bad echo";
        if (EchoConstant.QUERY_TIME.equals(body)) {
            responseBody = Calendar.getInstance().getTime().toString();
        }
        ByteBuf writeBuf = Unpooled.copiedBuffer(responseBody, CharsetUtil.UTF_8);
        ctx.write(writeBuf);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
