package com.lntea.netty.demo.diff.echo.aio;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * Title: AcceptCompletionHandler.java<br>
 * Description:  <br>
 * Copyright: Copyright (c) 2015<br>
 * Company: 北京云杉世界信息技术有限公司<br>
 *
 * @author lichao
 * @date 2019/11/5 17:50
 */
public class AcceptCompletionHandler implements CompletionHandler<AsynchronousSocketChannel,AioEchoServerHandler>{

    @Override
    public void completed(AsynchronousSocketChannel result, AioEchoServerHandler attachment) {
        attachment.getAsynchronousServerSocketChannel().accept(attachment, this);
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        result.read(buffer, buffer, new ReadCompletionHandler(result));
    }

    @Override
    public void failed(Throwable exc, AioEchoServerHandler attachment) {
        exc.printStackTrace();
        attachment.getLatch().countDown();
    }
}
