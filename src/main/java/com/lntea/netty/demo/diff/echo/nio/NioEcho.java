package com.lntea.netty.demo.diff.echo.nio;

import org.junit.Test;

/**
 * Title: NioEcho.java<br>
 * Description:  <br>
 * Copyright: Copyright (c) 2015<br>
 * Company: 北京云杉世界信息技术有限公司<br>
 *
 * @author lichao
 * @date 2019/11/5 10:38
 */
public class NioEcho {

    /** server ip */
    private final String SERVER_IP = "127.0.0.1";
    /** server port */
    private Integer SERVER_PORT = 8080;
    /** 查询时间约定字符串 */
    private final String QUERY_TIME = "query_time";

    @Test
    public void startEchoServer() {
        new Thread(new NioEchoServerHandler(SERVER_IP, SERVER_PORT, QUERY_TIME)).start();
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void startEchoClient() {
        new Thread(new NioEchoClientHandler(SERVER_IP, SERVER_PORT, QUERY_TIME)).start();
        try {
            Thread.currentThread().join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
