package com.lntea.netty.demo.diff.echo;

/**
 * Title: EchoConstant.java<br>
 * Description:  <br>
 * Copyright: Copyright (c) 2015<br>
 * Company: 北京云杉世界信息技术有限公司<br>
 *
 * @author lichao
 * @date 2019/11/5 18:09
 */
public interface EchoConstant {

    /** server ip */
    String SERVER_IP = "127.0.0.1";
    /** server port */
    Integer SERVER_PORT = 8080;
    /** 查询时间约定字符串 */
    String QUERY_TIME = "query_time";
}
