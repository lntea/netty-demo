package com.lntea.netty.demo.diff.echo.aio;

import org.junit.Test;

/**
 * Title: AioEcho.java<br>
 * Description:  <br>
 * Copyright: Copyright (c) 2015<br>
 * Company: 北京云杉世界信息技术有限公司<br>
 *
 * @author lichao
 * @date 2019/11/5 17:39
 */
public class AioEcho {

    @Test
    public void startEchoServer() {
        new Thread(new AioEchoServerHandler()).start();
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void startEchoClient() {
        new Thread(new AioEchoClientHandler()).start();
        try {
            Thread.currentThread().join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
